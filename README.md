# Gods Of The Arena

The project is divided in 2 different parts : the server and the client.
---------------
- First, install all the dependencies by running `npm install` in both server and client.
- Install mongodb if needed and launch `mongod` process
- `cd server` then `npm start` in a tab to launch the server - http://localhost:8081
- Same for the client - http://localhost:8082

- You can access the "Ludus" interface with route "/tselect"
- You can access a specific battle on the "Emperor" interface with route "/tselect/Emp/:id", :id being the db id for the battle

Description
---------------
This is a very poor attempt at the Gods of the Arena coding game. Keep whatever you think necessary to complete the objectives and discard/adapt the rest !