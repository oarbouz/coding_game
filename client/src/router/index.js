import Vue from 'vue'
import Router from 'vue-router'
import TSelect from '@/components/TypeSelect'
import NSelect from '@/components/NewSelect'
import ESelect from '@/components/EditSelect'
import MSelect from '@/components/Emperer'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/tselect',
      name: 'Tselect',
      component: TSelect
    },

    {
      path: '/tselect/new',
      name: 'NewSelect',
      component: NSelect
    },

    {
      path: '/tselect/:id',
      name: 'EditSelect',
      component: ESelect
    },

    {
      path: '/tselect/Emp/:id',
      name: 'EmpSelect',
      component: MSelect
    }
  ]
})
