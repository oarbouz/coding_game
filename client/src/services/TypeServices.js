import Api from '@/services/Api'

export default {
  fetchSelect () {
    return Api().get('tselect')
  },
  addSelect (params) {
    return Api().post('tselect', params)
  },


  updateSelect (params) {
    return Api().put('tselect/' + params.id, params)
  },

  getSelect (params) {
    return Api().get('tselect/' + params.id)
  },


  fetchTabSelect (params) {
    console.log('test TS');
    return Api().get('tselect/Emp/' + params.id);
  },

  addChoixSelect (params) {
    return Api().post('tselect/Emp/' + params.id)
  },

  deleteSelect (id) {
    return Api().delete('tselect/' + id)
  }
}
