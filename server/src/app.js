const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')

const app = express()
app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors())

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/tselect');
var db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", function(callback){
    console.log("Connection Succeeded");
});

var Select = require("../models/Tselect");

function InTab(tselect){
    var nb = 0;
    var index = 0;
    if(tselect.Archer == 'X'){
        nb++;
    }
    if(tselect.Chevalier== 'X'){
        nb++;
    }
    if(tselect.Epéiste == 'X' ){
        nb++;
    }
    if(tselect.Lancier == 'X'){
        nb++;
    }

    var tab = new Array(nb);
    if(tselect.Archer == 'X'){
        tab[index] = 'Archer';
        index++
    }
    if(tselect.Chevalier== 'X'){
        tab[index] = 'Chevalier';
        index++
    }
    if(tselect.Epéiste == 'X'){
        tab[index] = 'Epéiste';
        index++
    }
    if(tselect.Lancier == 'X'){
        tab[index] = 'Lancier';
    }
    return tab;
}

app.post('/tselect', (req, res) => {
    var db = req.db;
    var title = req.body.Title;
    var archer = req.body.Archer
    var chevalier = req.body.Chevalier;
    var epéiste  =req.body.Epéiste;
    var lancier = req.body.Lancier;
    var tab = InTab(req.body)
    var new_select = new Select({
        Title: title,
        Archer: archer,
        Chevalier: chevalier,
        Epéiste: epéiste,
        Lancier: lancier,
        Tab : tab
    })

    new_select.save(function (error) {
        if (error) {
            console.log(error)
        }
        res.send({
            success: true,
            message: 'Select saved'
        })
    })
})

app.post('/tselect/Emp/:id', (req, res) => {
    var archer = req.body.CArcher
    var chevalier = req.body.CChevalier;
    var epéiste  =req.body.CEpéiste;
    var lancier = req.body.CLancier;
    var new_selec = new Select({
        CArcher: archer,
        CChevalier: chevalier,
        CEpéiste: epéiste,
        CLancier: lancier,
    })

    new_selec.save(function (error) {
        if (error) {
            console.log(error)
        }
        res.send({
            success: true,
            message: 'saved'
        })
    })
})

app.get('/tselect', (req, res) => {
    Select.find({}, 'Title Archer Chevalier Epéiste Lancier', function (error, tselect) {
        if (error) { console.error(error); }
        res.send({
            tselect: tselect
        })
    })
})

app.get('/tselect/:id', (req, res) => {
    Select.findById(req.params.id, 'Title  Archer Chevalier Epéiste Lancier', function (error, tselect) {
        if (error) { console.error(error); }
        res.send(tselect)
    })
})


app.get('/tselect/Emp/:id', (req,res) => {
    console.log('test');
    Select.findById(req.params.id, 'Archer Chevalier Epéiste Lancier', function (error,mselect) {
        if (error) { console.error(error); }
        console.log('test mselect : ' + mselect);
        res.send(mselect);
    })
})



app.put('/tselect/:id', (req, res) => {
    var db = req.db;
    Select.findById(req.params.id, 'Title Archer Chevalier Epéiste Lancier Tab ', function (error, tselect) {
        if (error) { console.error(error); }

        tselect.Title = req.body.Title,
        tselect.Archer  = req.body.Archer,
        tselect.Chevalier = req.body.Chevalier,
        tselect.Epéiste  = req.body.Epéiste,
        tselect.Lancier = req.body.Lancier,
        tselect.Tab = req.body.Tab
        tselect.save(function (error) {
            if (error) {
                console.log(error)
            }
            res.send({
                success: true
            })
        })
    })
})

app.delete('/tselect/:id', (req, res) => {
    var db = req.db;
    Select.remove({
        _id: req.params.id
    }, function(err){
        if (err)
            res.send(err)
        res.send({
            success: true
        })
    })
})


app.listen(process.env.PORT || 8081)